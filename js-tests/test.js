
var openedThread = undefined;

var commentColor = d3.scale.linear()
 	.domain([1.0, 3.0, 5.0])
  	.range(["red", "yellow", "green"]);
  
// [id1, id2, ...]
function initTopics(threadIDs) {
	var root = d3.select("#threads").selectAll("div")
		.data(threadIDs)
		.enter()
	  .append("div")
		.attr("class", "thread")
		.attr("id", function(d) { return "thrid" + d; })
		.style("opacity", 0.0)
		.style("left", function (d) {
			var rect = document.getElementById("id" + d).getBoundingClientRect();
			var position = ((rect.left + rect.right) / 2 - 400);
			return Math.max(Math.min(document.body.clientWidth - 800, position), 0.0) + "px";
		})
		.style("top", function (d) {
			var rect = document.getElementById("id" + d).getBoundingClientRect();
			return (rect.bottom + 12) + "px";
		})
		.style("width", function(d) {
			return Math.min(document.body.clientWidth, 800) + "px";
		});
	
	// Title
	root.append("div")
		.attr("class", "ic_title")
		.text("COMMENTS:");
		
	root.append("div")
		.attr("class", "ic_close")
		.attr("align", "right")
		.text("×")
		.on("click", function(d) {
			d3.select("#thr" + openedThread)
			    .style("opacity", 0.0)
				.style("display", "none");
			openedThread = undefined;
		});
		
	// Other comments
	var comments = [
		{
			User: "User1",
			CommentText: "Test comment. Ok let me make it a bit longer. Another bit. Well even more.. should use at least two rows! So at the end it was even shorter than expected, I hope now it's enough!",
			Rating: 4.3
		},
		{
			User: "User2",
			CommentText: "Second comment.",
			Rating: 1.8
		}
	];
	
	
	var commentRoot = root.append("div").selectAll("div")
		.data(comments)
		.enter()
	  .append("div")
		.attr("class", "ic_comment")
		.style("background-color", function(d) { return commentColor(d.Rating); });
	
	// Add user and rating
	commentRoot.append("div")
		.attr("class", "ic_user")
		.text(function(d) { return d.User; })
	
	// Add comment
	commentRoot.append("div")
		.text(function(d) { return d.CommentText; });
		
	// Add new comment
	var form = root.append("form")
		.attr("class", "ic_form")
		.attr("id", "commentForm");
	
	form.append("textarea")
		.attr("type", "text")
		.attr("id", function(d) { return "newComment" + d; })
		.attr("placeholder", "Insert your comment and give a rating...")
		.style("width", "794px")
		.style("word-wrap", "break-word");
		
	var ratingDiv = form.append("div")
		.attr("class", "rating")
		.attr("align", "center");
		
	for (var i = 0; i < 5; i++) {
		ratingDiv.append("span")
			.text("☆")
			.on("click", function(d) {
				alert("TODO post comment. " + document.getElementById("newComment" + d).value);
			});
	}
	
	d3.selectAll(".commentsMark")
		.on("click", function() {
			var id = d3.select(this).attr("id");
			
			if (id == openedThread) return;
			if (openedThread != undefined) {
				d3.select("#thr" + openedThread)
			    	.style("opacity", 0.0)
					.style("display", "none");
			}
  			d3.select("#thr" + id)
			   	.style("opacity", 1.0)
				.style("display", "block");
			openedThread = id;
		});
}

function test_init() {
	var threads = [1, 2];
	initTopics(threads);
}