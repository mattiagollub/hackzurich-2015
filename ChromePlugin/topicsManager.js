
var openedTopic = undefined;

var commentColor = d3.scale.linear()
 	.domain([1.0, 3.0, 5.0])
  	.range(["red", "yellow", "green"]);
  
function injectHeaders() {
	var head = d3.select("head");
	head.append("style").text(
    "mark {" +
		"	background-color: rgba(255, 0, 0, 0.4);" +
		"	fill-opacity: 0.5;" +
		"	cursor: pointer;" +
		"	border-radius: 3px;" +
		"	padding: 0px 4px 0px 4px;" +
		"}" +
    "#topics, #topics * {" +
  	"animation : none;												  " + 
    "animation-delay : 0;											  " + 
    "animation-direction : normal;									  " + 
    "animation-duration : 0;										  " + 
    "animation-fill-mode : none;									  " + 
    "animation-iteration-count : 1;									  " + 
    "animation-name : none;											  " + 
    "animation-play-state : running;								  " + 
    "animation-timing-function : ease;								  " + 
    "backface-visibility : visible;									  " + 
    "background : 0;												  " + 
    "background-attachment : scroll;								  " + 
    "background-clip : border-box;									  " + 
    "background-color : transparent;								  " + 
    "background-image : none;										  " + 
    "background-origin : padding-box;								  " + 
    "background-position : 0 0;										  " + 
    "background-position-x : 0;										  " + 
    "background-position-y : 0;										  " + 
    "background-repeat : repeat;									  " + 
    "background-size : auto auto;									  " + 
    "border : 0;													  " + 
    "border-style : none;											  " + 
    "border-width : medium;											  " + 
    "border-color : inherit;										  " + 
    "border-bottom : 0;												  " + 
    "border-bottom-color : inherit;									  " + 
    "border-bottom-left-radius : 0;									  " + 
    "border-bottom-right-radius : 0;								  " + 
    "border-bottom-style : none;									  " + 
    "border-bottom-width : medium;									  " + 
    "border-collapse : separate;									  " + 
    "border-image : none;											  " + 
    "border-left : 0;												  " + 
    "border-left-color : inherit;									  " + 
    "border-left-style : none;										  " + 
    "border-left-width : medium;									  " + 
    "border-radius : 0;												  " + 
    "border-right : 0;												  " + 
    "border-right-color : inherit;									  " + 
    "border-right-style : none;										  " + 
    "border-right-width : medium;									  " + 
    "border-spacing : 0;											  " + 
    "border-top : 0;												  " + 
    "border-top-color : inherit;									  " + 
    "border-top-left-radius : 0;									  " + 
    "border-top-right-radius : 0;									  " + 
    "border-top-style : none;										  " + 
    "border-top-width : medium;										  " + 
    "bottom : auto;													  " + 
    "box-shadow : none;												  " + 
    "box-sizing : content-box;										  " + 
    "caption-side : top;											  " + 
    "clear : none;													  " + 
    "clip : auto;													  " + 
    "color : inherit;												  " + 
    "columns : auto;												  " + 
    "column-count : auto;											  " + 
    "column-fill : balance;											  " + 
    "column-gap : normal;											  " + 
    "column-rule : medium none currentColor;						  " + 
    "column-rule-color : currentColor;								  " + 
    "column-rule-style : none;										  " + 
    "column-rule-width : none;										  " + 
    "column-span : 1;												  " + 
    "column-width : auto;											  " + 
    "content : normal;												  " + 
    "counter-increment : none;										  " + 
    "counter-reset : none;											  " + 
    "cursor : auto;													  " + 
    "direction : ltr;												  " + 
    "display : block;												  " + 
    "empty-cells : show;											  " + 
    "float : none;													  " + 
    "font : normal;													  " + 
    "font-family : inherit;											  " + 
    "font-size : medium;											  " + 
    "font-style : normal;											  " + 
    "font-variant : normal;											  " + 
    "font-weight : normal;											  " + 
    "height : auto;													  " + 
    "hyphens : none;												  " + 
    "left : auto;													  " + 
    "letter-spacing : normal;										  " + 
    "line-height : normal;											  " + 
    "list-style : none;												  " + 
    "list-style-image : none;										  " + 
    "list-style-position : outside;									  " + 
    "list-style-type : disc;										  " + 
    "margin : 0;													  " + 
    "margin-bottom : 0;												  " + 
    "margin-left : 0;												  " + 
    "margin-right : 0;												  " + 
    "margin-top : 0;												  " + 
    "max-height : none;												  " + 
    "max-width : none;												  " + 
    "min-height : 0;												  " + 
    "min-width : 0;													  " + 
    "opacity : 1;													  " + 
    "orphans : 0;													  " + 
    "outline : 0;													  " + 
    "outline-color : invert;										  " + 
    "outline-style : none;											  " + 
    "outline-width : medium;										  " + 
    "overflow : visible;											  " + 
    "overflow-x : visible;											  " + 
    "overflow-y : visible;											  " + 
    "padding : 0;													  " + 
    "padding-bottom : 0;											  " + 
    "padding-left : 0;												  " + 
    "padding-right : 0;												  " + 
    "padding-top : 0;												  " + 
    "page-break-after : auto;										  " + 
    "page-break-before : auto;										  " + 
    "page-break-inside : auto;										  " + 
    "perspective : none;											  " + 
    "perspective-origin : 50% 50%;									  " + 
    "position : static;												  " + 
    "/* May need to alter quotes for different locales (e.g fr) */	  " + 
    "quotes : '\201C' '\201D' '\2018' '\2019';						  " + 
    "right : auto;													  " + 
    "tab-size : 8;													  " + 
    "table-layout : auto;											  " + 
    "text-align : inherit;											  " + 
    "text-align-last : auto;										  " + 
    "text-decoration : none;										  " + 
    "text-decoration-color : inherit;								  " + 
    "text-decoration-line : none;									  " + 
    "text-decoration-style : solid;									  " + 
    "text-indent : 0;												  " + 
    "text-shadow : none;											  " + 
    "text-transform : none;											  " + 
    "top : auto;													  " + 
    "transform : none;												  " + 
    "transform-style : flat;										  " + 
    "transition : none;												  " + 
    "transition-delay : 0s;											  " + 
    "transition-duration : 0s;										  " + 
    "transition-property : none;									  " + 
    "transition-timing-function : ease;								  " + 
    "unicode-bidi : normal;											  " + 
    "vertical-align : baseline;										  " + 
    "visibility : visible;											  " + 
    "white-space : normal;											  " + 
    "widows : 0;													  " + 
    "width : auto;													  " + 
    "word-spacing : normal;											  " + 
    "z-index : auto;												  " + 
    "}" +
		"#topics > .topic{" +
		"	font-family: 'Segoe UI', Arial, Helvetica, sans-serif;" +
		"	position: absolute;" +
  		"	z-index: 1070;" +
  		"	display: block;" +
		"	background-color: rgb(235, 235, 235);" +
		"	width: 800px;" +
		"	border-width: 1px;" +
		"	border-style: solid;" +
		"	border-color: rgb(200, 200, 200);" +
		"	border-radius: 2px;" +
		"	padding: 0px 10px 0px 10px;" +
		"	display: none;" +
		"}" +
		"#topics .ic_title {" +
		"	font-size: 18px;" +
		"	font-weight: 500;" +
		"	padding: 10px 0px 10px 0px;" +
		"	float: left;" +
		"}" +
		"#topics .ic_comment {" +
    " display: block;" +
		"	font-size: 14px;" +
		"	font-weight: 400;" +
		"	padding: 10px 10px 10px 10px;" +
		"	border-radius: 2px;" +
		"	margin-bottom: 1px;" +
		"}" +
		"#topics .ic_form {" +
		"	font-size: 14px;" +
		"	font-weight: 400;" +
		"	padding: 10px 0px 10px 0px;" +
		"}" +
		"#topics .ic_user {" +
		"	font-weight: 500;" +
		"}" +
		"#topics .ic_close {" +
		"	font-size: 30px;" +
		"	vertical-align: top;" +
		"	cursor: pointer;" +
    " text-align: right;" +
		"}" +
    "#topics .ic_upload {" +
		"	cursor: pointer;" +
    " text-align: right;" +
    " border-style: solid;" + 
    " border-width: 1px;" +
    " border-radius: 1px;" + 
    " float: right;" + 
    " padding: 4px;" + 
    " margin-bottom: 10px;" + 
		"}" +
		"#topics textarea {" +
		"	font-family: 'Segoe UI', Arial, Helvetica, sans-serif;" +
		"	height: 80px;" +
    " background-color: white;" +
    " padding: 4px;" + 
		"}" +
		"#topics .rating {" +
  	"	unicode-bidi: bidi-override;" +
 		"	direction: rtl;" +
    " text-align: center;" +
		"}" +
		"#topics .rating > span {" +
  	"	display: inline-block;" +
 		"	position: relative;" +
  	"	width: 1.1em;" +
		"	font-size: 35px;" +
		"}" +
		"#topics .rating > span:hover:before," +
		"#topics .rating > span:hover ~ span:before {" +
   	"content: \"\\2605\";" +
   	"	position: absolute;" +
		"}");
}
// [id1, id2, ...]
function initTopics(topicIDs, appendOnly) {
	
  if (!appendOnly) {
	  injectHeaders();
  }
  else {
    d3.select("#topics").remove();
  }
  
	d3.select("body").append("div")
	 .attr("id", "topics");
  
	var root = d3.select("#topics").selectAll("div")
		.data(topicIDs)
		.enter()
	  .append("div")
		.attr("class", "topic")
		.attr("id", function(d) { return "top" + d.topicId; })
		.style("opacity", 0.0)
		.style("left", function (d) {
			var rect = document.getElementById(d.topicId).getBoundingClientRect();
			var position = ((rect.left + rect.right) / 2 - 400);
			return Math.max(Math.min(document.body.clientWidth - 800, position), 0.0) + "px";
		})
		.style("top", function (d) {
			var rect = document.getElementById(d.topicId).getBoundingClientRect();
			return (rect.bottom + 12) + "px";
		})
		.style("width", function(d) {
			return (Math.min(document.body.clientWidth, 800) - 24) + "px";
		});
	
	// Title
	root.append("div")
		.attr("class", "ic_title")
		.text("COMMENTS:");
		
	root.append("div")
		.attr("class", "ic_close")
		.attr("align", "right")
		.text("×")
		.on("click", function(d) {
			d3.select("#top" + openedTopic)
			  .style("opacity", 0.0)
				.style("display", "none");
			openedTopic = undefined;
		});
		
	// Other comments
  var comments = root.append("div");
	var commentRoot = comments.append("div").selectAll("div")
		.data(function(d) { return d.comments; })
		.enter()
	  .append("div")
		.attr("class", "ic_comment")
		.style("background-color", function(d) { return commentColor(d.Rating); });
	
	// Add user and rating
	commentRoot.append("div")
		.attr("class", "ic_user")
		.text(function(d) { return d.UserName; })
	
	// Add comment
	commentRoot.append("div")
		.text(function(d) { return d.CommentText; });
		
	// Add new comment
	var form = root.append("form")
		.attr("class", "ic_form")
		.attr("id", "commentForm");
	
	form.append("textarea")
		.attr("type", "text")
		.attr("id", function(d) {
			return "newComment" + d.topicId; })
		.attr("placeholder", "Insert your comment and give a rating...")
		.style("width", "99%")
		.style("word-wrap", "break-word");
		
	var ratingDiv = form.append("div")
		.attr("class", "rating")
		.attr("align", "center");
    
  var dropboxDiv = form.append("div")
    .attr("class", "ic_upload")
    .text("UPLOAD TO DROPBOX")
    .on("click", function(d) {
      comments.append("div")
       .append("a")
        .attr("href", "http://www.dropbox.com")
        .attr("target", "_blank")
        .style("font-color", "blue")
        .style("text-decoration", "underline")
        .text("Dropbox attachement");
    });
		
	for (var i = 0; i < 5; i++) {
    ratingDiv.append("span")
			.text("☆")
      .attr("star", (5 - i).toString())
			.on("click", function(d) {
        var data = {
          TimeStamp: Date.now().toString(),
          Ip: "2.3.4.5",
				  CommentText: document.getElementById("newComment" + d.topicId).value,
          WebsiteRating: parseInt(d3.select(this).attr("star")),
          UserName: "TestUser" + Math.floor((Math.random() * 1000000) + 1),
          TargetText: d.TargetText,
          Url: d.Url
        };
        httpPostAsync(data);
        d3.select("#top" + openedTopic)
			   	.style("opacity", 0.0)
					.style("display", "none");
        openedTopic = undefined;
			});
	}
	
	d3.selectAll(".commentsMark")
		.on("click", function() {
			var id = d3.select(this).attr("id");
			
			if (id == openedTopic) return;
			if (openedTopic != undefined) {
				d3.select("#top" + openedTopic)
			    	.style("opacity", 0.0)
					.style("display", "none");
			}
  			d3.select("#top" + id)
			   	.style("opacity", 1.0)
				.style("display", "block");
			openedTopic = id;
		});
  if (appendOnly) {
    var id = topicIDs[topicIDs.length - 1].topicId;
    d3.select("#top" + id)
		  .style("opacity", 1.0)
			.style("display", "block");
		openedTopic = id;
  }
}