
// alert("Hello from your Chrome extension!");

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
      if(request.averageRating != null) {
        // alert("received average: " + request.averageRating);
        switch(request.averageRating){
            case 1:
              chrome.browserAction.setIcon ( { path: '/images/dark_red.jpg' } );
              break;
            case 2:
              chrome.browserAction.setIcon ( { path: '/images/red.jpg' } );
              break;
            case 3:
              chrome.browserAction.setIcon ( { path: '/images/orange.jpg' } );
              break;
            case 4:
              chrome.browserAction.setIcon ( { path: '/images/green.gif' } );
              break;
            case 5:
              chrome.browserAction.setIcon ( { path: '/images/dark_green.png' } );
              break;
            default:
              chrome.browserAction.setIcon ( { path: '/images/mario.png' } );
        }
      }
  });
 

chrome.contextMenus.create({
        title: "Comment",
        contexts:["selection"],
        onclick: clicked });
    
function clicked(info, tab) {
    // alert("Context menu clicked.");
    var selectedText = info.selectionText;
    
    chrome.tabs.query({active: true, currentWindow: true}, 
        function(tabs) { 
            chrome.tabs.sendMessage(tabs[0].id, 
            {
                "TargetText": selectedText
             },
                function(response) {
                    // alert(response.status);
                });
        }
    );
}


