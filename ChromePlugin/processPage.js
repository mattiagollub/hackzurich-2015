// alert(document.URL);
var lastTopicId = -1;
var oldTopics = [];

// Get data from db, inject code, call mattias function
var dbGetUrl = "http://hackzurichbackend.azurewebsites.net/api/comments/?url=";
var dbPostUrl = "http://hackzurichbackend.azurewebsites.net/api/comments/";

function buildPostMessage(targetText, url, timestamp, commentText, userName, rating){
  var message =  { "TargetText": targetText, "Url": url, "TimeStamp": timestamp, "CommentText": commentText,
                    "UserName": userName, "Rating": rating };
  return message;
}

var bogusData =     {
    "TargetText": "last",
    "Url": "ccc",
    "TimeStamp":"2015-10-10",
    "CommentText": "b",
    "UserName": "4",
    "Rating": "5"
  }

function httpGetAsync(callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) // 
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", dbGetUrl + document.URL, true); // TODO: true for asynchronous TODO: add page url
    xmlHttp.send(null);
}

// send GET request and process data
httpGetAsync(processResponse);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    newMarker(request.TargetText, document.URL);
    sendResponse({"status":"executed"});
  });

function httpPostAsync(content){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 1 && xhttp.status == 200) {
    }
  }
  
  xhttp.open("POST", dbPostUrl, true);
  xhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
  xhttp.send(JSON.stringify(content));
}


// read response message from server and output alert with text
function readResponse(databaseResponse){
  var resultArray = JSON.parse(databaseResponse);
  if(resultArray == null)
    resultArray = [];
  for(var i = 0; i < resultArray.length; ++i) {
    // alert(i);
    var data = " Url: " + resultArray[i].Url + " TargetText: " + resultArray[i].TargetText;
    data += " AverageRating: " + resultArray[i].AverageRating;
    // alert(data);
  }
}

function newMarker(targetText, url) {
  var topic = {
     TargetText: targetText,
     Url: url,
     comments: [],
     topicId: lastTopicId + 1
  };
  lastTopicId += 1;
  
  var endMarkerTag = "</mark>";
  var beginMarkerTag = "<mark id= \"" + lastTopicId + "\" class=\"commentsMark\">";
  var markerText = targetText;

  document.body.innerHTML = document.body.innerHTML.replace(
    markerText,
    beginMarkerTag + markerText + endMarkerTag);
  
  oldTopics.push(topic);
  initTopics(oldTopics, true);
}

// read response message from server, insert markers in dom and call visualazing function
// plus send the background activity
function processResponse(databaseResponse){
  var resultArray = JSON.parse(databaseResponse);
  if(resultArray == null)
    resultArray = [];
  var average = 0;
  var endMarkerTag = "</mark>";
  var nonAssignedComments = [];
  var assignedComments = [];
  // Inject markers
  for(var i = 0; i < resultArray.length; i++) {
    var beginMarkerTag = "<mark id= \"" + i + "\" class=\"commentsMark\">";
    var markerText = resultArray[i].TargetText;
    var oldHTML = document.body.innerHTML;
    document.body.innerHTML = document.body.innerHTML.replace(
    markerText,
    beginMarkerTag + markerText + endMarkerTag);
    // marked text not present in html code (probably because of dynmaico content)
    if(oldHTML == document.body.innerHTML){
      nonAssignedComments .push(resultArray[i]);
    }
    else {
      // insert topic id
      resultArray[i]["topicId"] = i;
      assignedComments .push(resultArray[i]);
      lastTopicId = i;
    }
    average += resultArray[i].AverageRating;
  }
  // TODO: send the background activity the data to display
  chrome.runtime.sendMessage(
    {"averageRating": Math.floor(average / resultArray.length)},
    function(response) { });
  
  // Call visualizing function
  oldTopics = assignedComments;
  initTopics(assignedComments, false);
}
