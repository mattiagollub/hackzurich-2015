﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hzbackend1.Models
{
    public class comment
    {
        //primary key
        public int Id { get; set; }

        //date
        public DateTime TimeStamp { get; set; }

        //ip
        public string Ip { get; set; }

        //url to the commented page
        public string Url { get; set; }

        //commented text
        public string TargetText { get; set; }

        //comment content
        public string CommentText { get; set; }

        //website rating
        public int WebsiteRating { get; set; }

        //user name
        public string UserName { get; set; }
       
        

    }
}