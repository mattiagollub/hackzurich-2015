﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hzbackend1.Models
{
    public class SimplifiedComment
    {
        public String CommentText { get; set; }
        public String UserName { get; set; }
        public int Rating { get; set; }
        public int Id { get; set; }
    }
}