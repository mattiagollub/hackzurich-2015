﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using hzbackend1.Models;

namespace hzbackend1.Models
{
    public class returnComment
    {
        public string TargetText { get; set; }
        public string Url { get; set; }
        public SimplifiedComment[] comments { get; set; }
        public int AverageRating { get; set; }
    }
}