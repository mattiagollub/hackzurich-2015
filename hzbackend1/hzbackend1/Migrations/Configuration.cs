namespace hzbackend1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using hzbackend1.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<hzbackend1.Models.hzbackend1Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(hzbackend1.Models.hzbackend1Context context)
        {
            context.comments.AddOrUpdate(p => p.Id,
                new comment 
                { 
                    Id = 1,
                    TimeStamp = DateTime.Now,
                    Url = "first.Url",
                    TargetText = "first.target",
                    CommentText = "first.comment",
                    Ip ="1.1.1.1",
                    WebsiteRating = -1,
                    UserName = "Anon"
                }
            );
        }
    }
}
