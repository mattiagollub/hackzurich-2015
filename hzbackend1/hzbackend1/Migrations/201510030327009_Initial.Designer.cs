// <auto-generated />
namespace hzbackend1.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Initial : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Initial));
        
        string IMigrationMetadata.Id
        {
            get { return "201510030327009_Initial"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
