﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hzbackend1.Models;

namespace hzbackend1.Controllers
{
    public class ViewCommentsController : Controller
    {
        private hzbackend1Context db = new hzbackend1Context();

        //
        // GET: /ViewComments/

        public ActionResult Index()
        {
            return View(db.comments.ToList());
        }

        //
        // GET: /ViewComments/Details/5

        public ActionResult Details(int id = 0)
        {
            comment comment = db.comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /ViewComments/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ViewComments/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(comment comment)
        {
            if (ModelState.IsValid)
            {
                db.comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(comment);
        }

        //
        // GET: /ViewComments/Edit/5

        public ActionResult Edit(int id = 0)
        {
            comment comment = db.comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /ViewComments/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comment);
        }

        //
        // GET: /ViewComments/Delete/5

        public ActionResult Delete(int id = 0)
        {
            comment comment = db.comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /ViewComments/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            comment comment = db.comments.Find(id);
            db.comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}