﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using hzbackend1.Models;
using hzbackend1.Services;
using System.Text;

namespace hzbackend1.Controllers
{
    public class commentsController : ApiController
    {
        private commentRepo _commentRepo;
 

        public commentsController()
        {
            this._commentRepo = new commentRepo();
        }

        public comment[] Get()
        {
           return _commentRepo.getAllComments();
        }


        public HttpResponseMessage Post(comment _comment)
        {
            if (this._commentRepo.SaveComment(_comment))
            {
                //XSockets
                var client = XSockets.Client40.ClientPool.GetInstance("WS://hackzurichbackend.cloudapp.net:8080", "HTTP://localhost");
                client.Send(_comment, "NEWPOST", "PostHandler");

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);
            }
 
            return Request.CreateResponse(System.Net.HttpStatusCode.ExpectationFailed);        
        }

 

        public HttpResponseMessage Get(string url)
        {
            try
            {
                //Find relevand comments
                var _responseComment = _commentRepo.getCommentByUrl(url);
                var response = Request.CreateResponse<returnComment[]>(System.Net.HttpStatusCode.OK, _responseComment);

                //Return
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return Request.CreateResponse(System.Net.HttpStatusCode.NotAcceptable);
            }

   
        }
    }
}
