﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using hzbackend1.Models;


namespace hzbackend1.Services
{
    public class commentRepo
    {
        private const string _CacheKey = "commentRepo";
        private hzbackend1Context _dbContext = new hzbackend1Context();


        
        public commentRepo() 
        {
            var _context = HttpContext.Current;



            //Seed for degugging

            if (_context != null)
            {
                if (_context.Cache[_CacheKey] == null)
                {
                    var _comments = new comment[]
                    {
                        new comment
                        {
                            Id = 1,
                            Url = "www.test.com",
                            TargetText = "some text target",
                            CommentText = "some comment text",
                            TimeStamp = DateTime.Now,
                            Ip = "1.1.1.1",
                            UserName = "Anon"
                        },
                        new comment
                        {
                            Id = 2,
                            Url = "2.test.com",
                            TargetText = "2some text target",
                            CommentText = "2some comment text",
                            TimeStamp = DateTime.Now,
                            Ip = "21.1.1.1",
                            UserName = "2Anon"
                        }
                    };
                    _context.Cache[_CacheKey] = _comments;
                }
            }
            //End of seed
     
        }

        public bool SaveComment(comment _comment)
        {
            try
            {
                _comment.TimeStamp = DateTime.Now;
                _dbContext.comments.Add(_comment);
                _dbContext.SaveChanges();


                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            //var _context = HttpContext.Current;

            //if (_context != null)
            //{
            //    try
            //    {
            //        //Append data
            //        _comment.Ip = "11.1.1.1.";
            //        _comment.TimeStamp = DateTime.Now;

            //        //Save data
            //        var currentData = ((comment[])_context.Cache[_CacheKey]).ToList();
            //        currentData.Add(_comment);
            //        _context.Cache[_CacheKey] = currentData.ToArray();

            //        return true;
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.ToString());
            //        return false;
            //    }
            //}


        }

        public returnComment[] getCommentByUrl(string _Url)
        {
            //Find relevant comments
            var _comments = _dbContext.comments.Where(p => p.Url == _Url);
            var _GroupedComments = _comments.GroupBy(p => p.TargetText);

            if (_GroupedComments.Count() > 0)
            {
                List<returnComment> _response = new List<returnComment>();
                //Build return object
                foreach (var _group in _GroupedComments)
                {
                    //A group is formed of comments addressing the same text
                    returnComment _TextGroup = new returnComment();
                    _TextGroup.TargetText = _group.Key;
                    _TextGroup.Url = _Url;

                    List<SimplifiedComment> _commentList = new List<SimplifiedComment>();
                    

                    foreach (var _comment in _group)
                    {
                        SimplifiedComment _tempComment = new SimplifiedComment();

                        _tempComment.CommentText = _comment.CommentText;
                        _tempComment.UserName = _comment.UserName;
                        _tempComment.Id = _comment.Id;
                        _tempComment.Rating = _comment.WebsiteRating;

                        _commentList.Add(_tempComment);
                    }

                    _TextGroup.comments = _commentList.ToArray();
                    _response.Add(_TextGroup);
                
                }

                return _response.ToArray();
            
            }
                   


            //Produce blank for debugging and error
            SimplifiedComment[] _blankResponse = new SimplifiedComment[1];
            _blankResponse[0] = new SimplifiedComment( ) 
                {
                    CommentText = "---",
                    UserName = "---",
                    Rating = -1,
                    Id = -1
                };
          
            return new returnComment[]
            {
                new returnComment
                {
                    TargetText = "xxx",
                    Url = "url",
                    comments = _blankResponse
                }
            
            };
            

            //return new comment[]
            //{
            //    //Returns blank comment
            //    new comment
            //    {
            //       Id = -1,
            //        Url = "---",
            //        TargetText = "---",
            //        CommentText = "---",
            //        TimeStamp = DateTime.Now,
            //        Ip = "---",
            //        UserName = "---"
            //    }
            //};
        }

        public comment[] getAllComments()
        {

            var context = HttpContext.Current;

            if (context != null)
            {
                return (comment[])context.Cache[_CacheKey];
            }

            return new comment[]
            {
                new comment
                {
                   Id = 0,
                    Url = "---",
                    TargetText = "---",
                    CommentText = "---",
                    TimeStamp = DateTime.Now,
                    Ip = "---",
                    UserName = "---"
                }
            };
        }
    }
}